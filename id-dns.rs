// id-dns: locate the servers responding to anycast DNS requests.
// Copyright 2019 Zack Weinberg <zackw@panix.com> &
//                Shinyoung Cho <cho.grace71@gmail.com>
//
// This program is free software:  you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.  See the file LICENSE in the
// top level of the source tree containing this file for further details,
// or consult <https://www.gnu.org/licenses/>.

const ABOUT_CMD: &str = r#"
Use Akamai's "whoami" service to determine the IP addresses of the
physical servers responding to DNS queries sent to anycast addresses.
"#;

//
// Imports
//
#[macro_use] extern crate failure;

use std::io;
use std::net::IpAddr;
use std::vec::Vec;

use failure::{Error, ResultExt};
use maxminddb::Reader as GeoDB;
use structopt::StructOpt;
use tokio::runtime::current_thread::Runtime;
use trust_dns_resolver::lookup::TxtLookup;

//
// Main loop
//

/// Error type for process_response.
#[derive(Debug, Fail)]
#[fail(display="No \"ns\" record in response")]
struct NoNSRecordError;

fn parse_ip_in_txt(value: &[u8]) -> Result<IpAddr, Error> {
    let value = std::str::from_utf8(value.as_ref())?;
    let value = value.parse::<IpAddr>()?;
    Ok(value)
}

fn process_response(resolver: IpAddr, response: TxtLookup)
                    -> (IpAddr, Result<IpAddr, Error>) {

    for txt in response {
        if let [tag, value] = txt.txt_data() {
            if tag.as_ref() == b"ns" {
                return (resolver, parse_ip_in_txt(value));
            }
        }
    }
    (resolver, Err(NoNSRecordError.into()))
}

fn run_queries(args: CommandArgs, runtime: &mut Runtime)
               -> Vec<(IpAddr, Result<IpAddr, Error>)> {

    use std::convert::Infallible;
    use trust_dns_resolver::AsyncResolver;
    use trust_dns_resolver::config::*;
    use futures::future::Future;
    use futures::stream::{Stream, FuturesUnordered};

    // Pending queries.
    let mut queries: FuturesUnordered<Box<Future<
            Item=(IpAddr, Result<IpAddr, Error>), Error=Infallible>>>
        = FuturesUnordered::new();

    for addr in args.resolvers {
        let (client, task) = AsyncResolver::new(
            ResolverConfig::from_parts(
                None, vec![], NameServerConfigGroup::from_ips_clear(
                    &[addr], 53)),
            ResolverOpts::default());

        runtime.spawn(task);
        queries.push(Box::new(
            client.txt_lookup("whoami.ds.akahelp.net.")
                .then(move |result| match result {
                    Ok(response) => Ok(process_response(addr, response)),
                    Err(e) => Ok((addr, Err(e.into())))
                })
        ));
    }

    runtime.block_on(queries.collect()).unwrap()
}

fn lookup_geoip(db: &GeoDB, resolver: IpAddr, addr: IpAddr)
                -> Result<[String; 5], Error> {

    use maxminddb::MaxMindDBError::AddressNotFoundError as ANF;

    let city: maxminddb::geoip2::City = db.lookup(addr)?;

    let location = city.location.ok_or_else(
        || ANF(String::from("location missing")))?;
    let longitude = location.longitude.ok_or_else(
        || ANF(String::from("longitude missing")))?;
    let latitude = location.latitude.ok_or_else(
        || ANF(String::from("longitude missing")))?;

    let country = city.registered_country.ok_or_else(
        || ANF(String::from("registered country missing")))?;
    let ccode = country.iso_code.ok_or_else(
        || ANF(String::from("longitude missing")))?;

    Ok([resolver.to_string(), addr.to_string(), ccode,
        longitude.to_string(), latitude.to_string()])
}

// Returns Result<exit_code_for_main, Error>
//
// The error is an IO error or another fatal condition
//
// A non-zero exit_code_for_main means that an error occurred during
// lookups, but otherwise everything ran correctly and the whole
// list of successful/failed lookups was printed.
fn inner_main(args: CommandArgs) -> Result<i32, Error> {
    use std::io::{LineWriter, Write};

    // lock and line-buffer stderr (eprintln! doesn't line-buffer, which is
    // probably a bug, but we can work around it for now).
    let stderr = io::stderr();
    let stderr = stderr.lock();
    let mut stderr = LineWriter::new(stderr);

    // Try to read the geoip database, if any.
    // This uses match blocks so it can return on error.
    let geodb = match args.geoip.as_ref() {
        None => None,
        Some(s) => Some(GeoDB::open(s).context(fmt!("could not open GeoDB {}", s))?),
    };

    // Do all the actual queries, then tear down the network stack.
    let mut runtime = Runtime::new()?;
    let results = run_queries(args, &mut runtime);
    runtime.run().context("runtime didn't run successfully")?;

    // Lock and buffer stdout.
    let stdout = io::stdout();
    let stdout = stdout.lock();
    let mut stdout = csv::Writer::from_writer(stdout);

    if geodb.is_none() {
        stdout.write_record(&["resolver", "address"])?;
    } else {
        stdout.write_record(
            &["resolver", "address", "country", "longitude", "latitude"]
        )?;
    }

    // Produce results.
    let mut status = 0;

    for (resolver, result) in results {
        match result {
            Err(e) => {
                status = 1;
                writeln!(stderr, "id-dns: {}: {}", resolver, e)?;
            },
            Ok(addr) => match geodb {
                None => {
                    stdout.write_record(&[resolver.to_string(), addr.to_string()])?;
                },
                Some(ref db) => match lookup_geoip(db, resolver, addr) {
                    Err(e) => {
                        status = 1;
                        writeln!(stderr, "id-dns: {}->{}: {}", resolver, addr, e)?;
                    },
                    Ok(record) => {
                        stdout.write_record(&record)?;
                    }
                }
            }
        }
    }

    stdout.flush()?;

    Ok(status)
}

//
// Command line parsing.
//
#[derive(Debug, StructOpt)]
#[structopt(
    name="id-dns",
    raw(about="ABOUT_CMD"),
    raw(setting="structopt::clap::AppSettings::DeriveDisplayOrder"),
    raw(setting="structopt::clap::AppSettings::UnifiedHelpMessage"),
)]
struct CommandArgs {
    /// Anycast addresses of public DNS resolvers; a request to Akamai's
    /// whoami service will be sent via each.
    #[structopt(required=true)]
    resolvers: Vec<IpAddr>,

    /// If provided, look up the results of the whoami queries in this
    /// MaxMind-style geoip database and report latitude, longitude,
    /// country code, and city name as well as the IP address of each.
    /// This should be a PathBuf, but maxminddb::open takes a &str.
    #[structopt(short="g", long="geoip")]
    geoip: Option<String>
}

fn main() {
    match inner_main(CommandArgs::from_args()) {
        Ok(code) => std::process::exit(code),

        Err(e) => {
            // You may have to fiddle here to print the context; see the failure docs
            writeln!(io::stderr(), "id-dns: {}", e).unwrap();
            std::process::exit(1);
        }
    }
}
